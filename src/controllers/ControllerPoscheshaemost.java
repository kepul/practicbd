package controllers;

import database.DataBase;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.sql.SQLException;

/**
 * Класс контроллер для окна заполнения данных таблицы poshechaemost
 * @author А. Сидорук
 */
public class ControllerPoscheshaemost {

    private final DataBase dataBase = new DataBase();
    public TextField student_code;
    public TextField date;
    public TextField presented;

    /**
     * При нажатие на кнопку происходит передача данных из полей окна в запрос в таблицу poshechaemost
     * @throws SQLException
     */
    @FXML
    public void insertData() throws SQLException {
        dataBase.insertIntoPoscheshaemost(Integer.parseInt(student_code.getText()), date.getText(), presented.getText());
    }
}
