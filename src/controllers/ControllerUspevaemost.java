package controllers;

import database.DataBase;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.sql.SQLException;

/**
 * Класс контроллер для окна заполнения данных таблицы uspevaemost
 * @author А. Сидорук
 */
public class ControllerUspevaemost {

    private final DataBase dataBase = new DataBase();
    public TextField student_code;
    public TextField ocenka;
    public TextField predmet;

    /**
     * При нажатие на кнопку происходит передача данных из полей окна в запрос в таблицу uspevaemost
     * @throws SQLException
     */
    @FXML
    public void insertData() throws SQLException {
        dataBase.insertIntoUspevaemost(Integer.parseInt(student_code.getText()), Integer.parseInt(ocenka.getText()), predmet.getText());
    }
}