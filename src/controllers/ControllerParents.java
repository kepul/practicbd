package controllers;

import database.DataBase;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class ControllerParents {

    private final DataBase dataBase = new DataBase();
    public TextField student_code;
    public TextField full_name_moms;
    public TextField full_name_dad;
    public TextField home_address_dad;
    public TextField contact_number_dad;
    public TextField dad_place_of_work;
    public TextField home_address_moms;
    public TextField contact_number_moms;
    public TextField moms_place_of_work;

    @FXML
    void insertData() throws SQLException {
        dataBase.insertIntoParents(Integer.parseInt(student_code.getText()), full_name_moms.getText(), home_address_moms.getText(), contact_number_moms.getText(),
                moms_place_of_work.getText(), full_name_dad.getText(), home_address_dad.getText(), contact_number_dad.getText(), dad_place_of_work.getText());
    }
}
