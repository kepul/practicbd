package database;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Класс для работы с БД
 * @author А. Сидорук
 */
public class DataBase implements MySQLDataBaseConfig {

    private static Connection connection;

    static {
        try {
            connection = getConnection();
        } catch (Exception ignored) {
        }
    }

    private static Connection getConnection() throws Exception {
        Connection connection;

        Class.forName(DRIVER_NAME).getDeclaredConstructor().newInstance();
        connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

        return connection;
    }

    public void closeConnection() throws SQLException {
        connection.close();
    }

    /**
     * Отправляет запрос на заполнение таблицы parents в БД
     *
     * @param student_code код студента
     * @param full_name_moms ФИО мамы
     * @param home_address_moms домашний адрес мамы
     * @param contact_number_moms контактный номер мамы
     * @param moms_place_of_work рабочий номер мамы
     * @param full_name_dad ФИО отцы
     * @param home_address_dad домашний адрес папы
     * @param contact_number_dad контактный номер папы
     * @param dad_place_of_work рабочий номер папы
     * @throws SQLException
     */
    public void insertIntoParents(int student_code,
                                  String full_name_moms, String home_address_moms, String contact_number_moms, String moms_place_of_work,
                                  String full_name_dad, String home_address_dad, String contact_number_dad, String dad_place_of_work) throws SQLException {

        String query = "INSERT INTO `parents` (`student_code`, `full_name_moms`, `home_address_moms`, " +
                "`contact_number_moms`, `moms_place_of_work`, `full_name_dad`, `home_address_dad`, `contact_number_dad`, `dad_place_of_work`) VALUES (" +
                "\"" + student_code + "\", " +
                "\"" + full_name_moms + "\", " +
                "\"" + home_address_moms + "\", " +
                "\"" + contact_number_moms + "\", " +
                "\"" + moms_place_of_work + "\", " +
                "\"" + full_name_dad + "\", " +
                "\"" + home_address_dad + "\", " +
                "\"" + contact_number_dad + "\", " +
                "\"" + dad_place_of_work + "\");";

        Statement statement = connection.createStatement();
        statement.executeUpdate(query);
    }

    /**
     * Отправляет запрос на заполнение таблицы students в БД
     *
     * @param student_code код студента
     * @param full_name_student ФИО студента
     * @param date_of_birth дата рождения
     * @param home_address домашний адрес
     * @param contact_number контактный телефон
     * @throws SQLException
     */
    public void insertIntoStudents(int student_code, String full_name_student, String date_of_birth, String home_address, String contact_number) throws SQLException {

        String query = "INSERT INTO `students` (`student_code`, `full_name_student`, `date_of_birth`, `home_address`, `contact_number`) VALUES (" +
                "'" + student_code + "', " +
                "\"" + full_name_student + "\", " +
                "\"" + date_of_birth + "\", " +
                "\"" + home_address + "\", " +
                "\"" + contact_number + "\");";

        Statement statement = connection.createStatement();
        statement.executeUpdate(query);

    }

    /**
     * Отправляет запрос на заполнение таблицы personal_data в БД
     *
     * @param student_code код студента
     * @param passport_data номер паспорта
     * @param TIN_number_SNILS номер СНИЛС
     * @throws SQLException
     */
    public void insertIntoPersonalData(int student_code, int passport_data, int TIN_number_SNILS) throws SQLException {

        String query = "INSERT INTO personal_data (`student_code`, `passport_data`, `TIN_number_SNILS`) VALUES (" +
                "'" + student_code + "', " +
                "'" + passport_data + "', " +
                "'" + TIN_number_SNILS + "');";

        Statement statement = connection.createStatement();
        statement.executeUpdate(query);
    }

    /**
     * Отправляет запрос на заполнение таблицы poscheshaemost в БД
     * @param student_code код студента
     * @param date дата
     * @param presented стутус посещения
     */
    public void insertIntoPoscheshaemost(int student_code, String date, String presented) throws SQLException {

        String query = "INSERT INTO `poscheshaemost` (`student_code`, `date`, `presented`) VALUES (" +
                "'" + student_code + "', " +
                "'" + date + "', " +
                "'" + presented + "');";

        Statement statement = connection.createStatement();
        statement.executeUpdate(query);
    }

    /**
     * Отправляет запрос на заполнение таблиц uspevaemost в БД
     *
     * @param student_code код студента
     * @param ocenka оценка
     * @param predmet предмет
     * @throws SQLException
     */
    public void insertIntoUspevaemost(int student_code, int ocenka, String predmet) throws SQLException {

        String query = "INSERT INTO uspevaemost (`student_code`, `ocenka`, `predmet`) VALUES (" +
                "'" + student_code + "', " +
                "'" + ocenka + "', " +
                "'" + predmet + "');";

        Statement statement = connection.createStatement();
        statement.executeUpdate(query);
    }

    /**
     * Возвращает список всех таблиц БД
     *
     * @return список таблиц БД
     * @throws SQLException
     */
    public ArrayList<String> getTableList() throws SQLException {
        ArrayList<String> tableList = new ArrayList<>();
        String query = "SELECT `TABLE_NAME` FROM `information_schema`.`TABLES` WHERE `TABLES`.`TABLE_SCHEMA` = 'training_practice_2';";

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        int columns = resultSet.getMetaData().getColumnCount();

        while (resultSet.next()) {
            for (int i = 1; i <= columns; i++) {
                tableList.add(resultSet.getString(i));

            }
        }

        return tableList;
    }

    /**
     * Возвращает список названий столбцов в таблице
     *
     * @param table имя таблицы
     * @return список названий столбцов
     * @throws SQLException
     */
    public ArrayList<String> getColumnList(String table) throws SQLException {
        ArrayList<String> tableField = new ArrayList<>();
        String query = "SHOW COLUMNS FROM `" + table + "`";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            tableField.add(resultSet.getString(1));
        }

        return tableField;
    }

    /**
     * Возвращает все данные таблицы БД
     *
     * @param tableName имя таблицы
     * @param columnCount кол-во столбцов в таблице
     * @param columnsNames список имён столбцов
     * @return данные из таблицы
     * @throws SQLException
     */
    public ObservableList<HashMap> getInfo(String tableName, int columnCount, ArrayList<String> columnsNames) throws SQLException {
        return FXCollections.observableArrayList(readData(tableName, columnCount, columnsNames));
    }

    private ArrayList<HashMap> readData(String tableName, int columnCount, ArrayList<String> columnNames) throws SQLException {

        ArrayList<HashMap> hashMaps = new ArrayList<>();

        String query = "SELECT * FROM `" + tableName + "`";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            HashMap<String, String> dataBaseInfo = new HashMap<>();
            for (int i = 1; i <= columnCount; i++) {
                dataBaseInfo.put(columnNames.get(i - 1), resultSet.getString(i));
            }
            hashMaps.add(dataBaseInfo);
        }

        resultSet.close();

        return hashMaps;
    }

    /**
     * Возвращает количество столбцов в конкретной таблицы
     * @param table имя таблицы
     * @return кол-во столбцов в таблице
     * @throws SQLException
     */
    public int getColumnCount(String table) throws SQLException {
        ArrayList<String> tableField = new ArrayList<>();

        String query = "SHOW COLUMNS FROM `" + table + "`";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            tableField.add(resultSet.getString(1));
        }

        return tableField.size();
    }


}