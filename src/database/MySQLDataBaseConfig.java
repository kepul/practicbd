package database;

public interface MySQLDataBaseConfig {

    String URL = "jdbc:mysql://localhost:3306/training_practice_2?serverTimezone=Europe/Moscow&useSSL=false";
    String USERNAME = "root";
    String PASSWORD = "";
    String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
}